#pragma once

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/aruco.hpp>

#include "camera_integration.h"

class ArucoDetector
{
private:
	static ArucoDetector *_singleton;

public:
	CameraIntegration *_cam;
	// Vector3f _detected_pose_from_marker;
	float _marker_size; // TEST: 0.1778

	// Output
	uint32_t _detected_marker_id;
	bool _marker_found;

	// Aruco parameters
	cv::Ptr<cv::aruco::DetectorParameters> _detector_params;
	cv::Ptr<cv::aruco::Dictionary> _dictionary;

	// Target position and orientation
	bool _target_set;
	cv::Vec3d _target_position;
	cv::Vec3d _target_orientation;

	ArucoDetector(float marker_size)
	{
		_singleton = this;
		_cam = CameraIntegration::get_singleton();
		_marker_size = marker_size;

		_dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_ARUCO_ORIGINAL);
		_detector_params = cv::aruco::DetectorParameters::create();
		// _detector_params->cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX;
		_detector_params->adaptiveThreshWinSizeMin = 5;
		_detector_params->adaptiveThreshWinSizeMax = 21;

		_marker_found = false;

		_target_set = false;
	}

	static ArucoDetector *get_singleton()
	{
		return _singleton;
	}

	void update();

	static void getEulerAngles(cv::Vec3d &rotCamerMatrix, cv::Vec3d &eulerAngles)
	{
		cv::Mat rotCamerMatrix1;
		cv::Rodrigues(rotCamerMatrix,rotCamerMatrix1);
		cv::Mat cameraMatrix,rotMatrix,transVect,rotMatrixX,rotMatrixY,rotMatrixZ;
		double* _r = rotCamerMatrix1.ptr<double>();
		double projMatrix[12] = {_r[0],_r[1],_r[2],0, _r[3],_r[4],_r[5],0, _r[6],_r[7],_r[8],0};
		cv::decomposeProjectionMatrix(cv::Mat(3,4,CV_64FC1,projMatrix), cameraMatrix, rotMatrix, transVect, rotMatrixX, rotMatrixY, rotMatrixZ, eulerAngles);
	}
};