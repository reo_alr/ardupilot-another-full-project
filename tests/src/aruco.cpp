#include "aruco.h"

void ArucoDetector::update()
{
	cv::Mat image = _cam->_post_image.clone();
	cv::cvtColor(image, image, cv::COLOR_RGB2BGR);
	std::vector<int> ids;
	std::vector<std::vector<cv::Point2f>> corners, rejected_corners;

	cv::aruco::detectMarkers(image, _dictionary, corners, ids, _detector_params, rejected_corners);

	// Check whether we get any marker

	if(ids.empty())
	{
		return;
	}

	// Get only one marker
	
	_detected_marker_id = ids.front();
	// std::cout << "hurah22?" << std::endl;
	std::vector<cv::Point2f> detected_marker_corners = corners.front();

	// Estimate pose from one marker

	std::vector<cv::Vec3d> rotation_vector, translation_vector;

	cv::aruco::estimatePoseSingleMarkers(corners, _marker_size, _cam->_intrinsic_matrix, _cam->_distortion_coefficient, rotation_vector, translation_vector);

	// calculate positional coordinate of the marker

	cv::Matx33d rotation_matrix = cv::Matx<double, 3, 3>(0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0);
	cv::Rodrigues(rotation_vector, rotation_matrix);
	cv::Mat res = -1 * (cv::Mat)rotation_matrix;
	cv::Mat res2;
	cv::transpose(res, res2);
	res = res2 * cv::Mat(translation_vector.front());

	cv::Vec3d coordinate(res);

	// TODO: Get target attitude
	// TEST: set 0,0,0 as drone's position and orientation
	cv::Vec3d self_pos(0.0, 0.0, 0.0);
	cv::Vec3d self_rot(0.0, 0.0, 0.0);

	_target_position = self_pos - coordinate;

	std::cout << translation_vector.front() << std::endl;
	std::cout << coordinate << std::endl;

	ArucoDetector::getEulerAngles(rotation_vector.front(), self_rot);

	_target_orientation = self_rot;
	_target_set = true;
}

ArucoDetector *ArucoDetector::_singleton;