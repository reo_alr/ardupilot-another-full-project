#include "aruco.h"
#include "camera_integration.h"

int main()
{
	CameraIntegration ci(0);
	ArucoDetector ad(0.1778);

	ci.setCameraInfo(800, 600, 0.0, 0.0, 0.0, 0.0, 1.0, 1.0, 1.0);

	while(true)
	{
		gazebo::common::Time::MSleep(10);
		ci.update();
		if (! ci._post_image_set)
		{
			continue;
		}
		ad.update();
		cv::imshow("camera", ci._post_image);
		cv::waitKey(1);
		if (ad._target_set)
		{
			// std::cout << ad._target_position << std::endl;
			// std::cout << ad._target_orientation << std::endl;
			std::cout << std::endl;
		}
		// gazebo::common::Time::MSleep(10);
	}

	gazebo::client::shutdown();

	return 0;
}