# Ardupilot-Another

## Directories / Packages

*  `ardupilot/` - The Ardupilot Project, forked from https://github.com/ArduPilot/ardupilot . Edited to do a marker detection with OpenCV and integrating camera sensor (which from Gazebo plugins), and added flight mode to land based on marker position.
*  `ardupilot_gazebo/` - Gazebo plugins to accomodate Ardupilot SITL, forked from https://github.com/khancyr/ardupilot_gazebo . Edited to have a camera plugin that stream images straight to a specific gazebo topic.
*  `gazebo_env/` - Gazebo world with Aruco Marker, adapted from https://github.com/AerialRobotics-IITK/aruco_detection_gazebo .
*  `tests/` - A test application for Camera stream from Gazebo and Marker Detection.

## Alteration

`NEW_Camera` and `NEW_Aruco` directories are added in libraries of Ardupilot. NEW_Camera is the camera integration, which reads gazebo topic. NEW_Aruco is is a middleware to process the image taken from NEW_Camera module.

To accomodate OpenCV and Gazebo library, wscript on Ardupilot root folder and the one on ArduCopter subfolder is edited to link the libraries.

`mode_markerland.cpp` in ArduCopter subfolder contains the behavior of the new flight mode that is MarkerLand, which makes the drone land based on the marker location.

Some alteration are added on scheduling system on `Copter.cpp` to accomodate new libraries and new flight mode.

More information about the addition inside the individual file could be traced by finding "addition" comments on the code. I put this comment on almost every alteration made in the code.

## Notes

### Libraries, Modules and Scheduler

Before we integrate Camera and Aruco Marker Detector, we have to understand how some libraries inside Ardupilot works. A library consist a class, the class consists of methods and variables that are used for various purpose, if it's a sensor library, the class definition defines the communication line between the sensor itself and Ardupilot program. These class can be instantinated to become an object, or in a simpler term: a module inside Ardupilot code. These module should be activated when the Ardupilot boot up, in Copter case, the initialize system sequence can be found in `ArduCopter/system.cpp` in the `init_ardupilot()` function. These modules may have their own process, so we may define their process in the Scheduler, in Copter case, we can add an entry to the scheduler in `ArduCopter/Copter.cpp`. Scheduler is basically a module inside Ardupilot to manage the internal loop of the system, adding our module's loop function to the scheduler will make that function being run in the loop so that the module will work as intended.

To accomodate this model, module writer should use the scheduler by creating an 'update' function, for example, in my case, the camera subscribe to the Gazebo camera plugin topic, the update function for this module would be: create a copy of the image from the camera to a variable so all other module inside Ardupilot can retrieve the image, and make sure that the image is not corrupted by doing a write/read lock to the variable. The next example in my case is Aruco Marker Detection, the update function for this module would be detecting a marker and report the position of the marker to a variable that all other module can read. Integrating these function inside scheduler will make sure the image from camera is updated and the position of the marker inside the image is always reported every time the system is looping.

### Flight Mode

Ardupilot is documented on how to adding a new flight mode in the Ardupilot: http://ardupilot.org/dev/docs/apmcopter-adding-a-new-flight-mode.html

This part of the code is actually the heart of the control of the vehicle and actually have a well coded and well defined relationship between this part of the code and the attitude control. Although we may use Waypoint / positional based vehicle control if the GPS is enabled (see `ArduCopter/mode_guided.cpp`).

Vehicle can be controled by giving the vehicle a velocity command, attitude command (roll, pitch, yaw), positional target (x, y, z of local coordinate or using geodetic coordinate).

#### Mode MarkerLand

My new flight mode use Aruco Marker Detector to get the position of the marker. The `ArduCopter/mode_markerland.cpp` read the position of the marker, as the information regarding the position of marker is available for all module in the code. Using the information, the function inside `ArduCopter/mode_markerland.cpp` command the drone to approach the marker position by using attitude command. If the drone position is on the top of the marker, the code command the drone to land using the already defined behaviour of flight mode "Land" (`ArduCopter/mode_land.cpp`). We can actually use function from another mode, but only a function that came from the mode class that we inherit our class from. For example mode Guided_NoGPS is the child of mode Guided, so Mode Guided_NoGPS is inheriting some function from the original Guided Mode. My MarkerLand Mode is inheriting Land Mode.

### Building the code

Waf build system is not an ordinary build system such as a traditional Makefile, it's written in Python and have a different behavior. Although it has some parallel with cmake generation script, to build a code, we need to find the package / libraries that we use in our project, and then we link the library in the building process.

Waf system seperate the compilation phase into configure phase and build phase. Configure phase consists of configuring the compilation tool and the library / include path used in the project. Build phase consists of the building code process.

To accomodate the code dependency, that is Gazebo library and OpenCV library, we need edit the `wscript` file. We add `check_cfg()` function to find our required libraries, and then we add them to a variable with `uselib_store=` parameter, so that we can reference these packages back from build phase of the compilation for linking purpose. After that we add include path for the project, we use `env.append_value("INCLUDES", ["/usr/lib/...", ...])` to define our include paths. Also we need to add `env.append_value('CXXFLAGS', '-fexceptions')` to avoid some error in the compilation process.

In the build phase, we add some code in `wscript` file inside `ArduCopter/` subdirectory. First we add our new library that is "NEW_Camera" and "NEW_Aruco" in the `bld.ap_stlib(..., ap_libraries=bld.ap_common_vehicle_libraries() + [<all required libraries including ours>])`. Lastly, we add our required library (Gazebo and OpenCV) in the `use` attirbute in `bld.ap_program()` function. We add the variable that we define before in `check_cfg()` function.

### Failsafes

Turns out that failsafes in Ardupilot are not related to each other. If we want to define our own failsafe, we can just define it as a "restriction" of any command that shouldn't be done in that particular failsafe event. There is no real definition or convention on how a new failsafe should be added. There is no real interface between the bare code and the class relationship that are presented like how new flight mode is added or how a function execution is added to scheduler. The only time that failsafes are related to each other is when the GCS issued a query whether there is a failsafe event occured, which checks every failsafes listed in failsafe object (created from an anonymous struct) in `Copter.h`