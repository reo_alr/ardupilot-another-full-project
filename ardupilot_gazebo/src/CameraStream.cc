#include <gazebo/gazebo.hh>
#include <gazebo/physics/physics.hh>
#include <gazebo/plugins/CameraPlugin.hh>
#include <gazebo/transport/transport.hh>

#include <string>

namespace gazebo
{
	class CameraStreamPlugin : public gazebo::CameraPlugin
	{
		const unsigned char *_image_ptr;
		gazebo::transport::NodePtr node;
		gazebo::transport::PublisherPtr publisher;

		public:void Load(gazebo::sensors::SensorPtr _parent, sdf::ElementPtr _sdf)
		{

			node = gazebo::transport::NodePtr(new gazebo::transport::Node());
			node->Init("default");
			publisher = node->Advertise<gazebo::msgs::Image>("~/fake_streaming");
			gazebo::CameraPlugin::Load(_parent, _sdf);
			this->parentSensor->SetActive(true);
		}

		void OnNewFrame(const unsigned char * _image, unsigned int _width, unsigned int _height, unsigned int _depth, const std::string &_format)
		{
			gazebo::msgs::Image fakeMsg;
			_image_ptr=_image;
			fakeMsg.set_width(_width);
			fakeMsg.set_height(_height);
			fakeMsg.set_pixel_format((common::Image::PixelFormat)3);
			fakeMsg.set_data(_image_ptr, _width*_height*3);
			fakeMsg.set_step(0);
			publisher->Publish(fakeMsg);
		}
	};
}

GZ_REGISTER_SENSOR_PLUGIN(gazebo::CameraStreamPlugin)