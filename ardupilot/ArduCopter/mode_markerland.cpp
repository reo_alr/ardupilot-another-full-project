#include "Copter.h"

bool Copter::ModeMarkerLand::init(bool ignore_checks)
{
	_on_top_of_marker = false;
	if(! copter.ad._marker_found)
	{
		return false;
	}
	return true;
}

void Copter::ModeMarkerLand::run()
{
	if(_on_top_of_marker)
	{
		Copter::ModeLand::nogps_run();
		return;
	}
	if (! copter.ad._marker_found)
	{
		// drone has lost marker vision
		gcs().send_text(MAV_SEVERITY_INFO, "LOST VISION ON MARKER!");
		attitude_control->input_euler_angle_roll_pitch_yaw(0.0, 0.0, 0.0, true);
		pos_control->update_z_controller();
	}
	// float _x_real = copter.ad._target_position[0];
	// float _y_real = copter.ad._target_position[1];
	// uint8_t _disable_pitch, _disable_roll;
	float dist_x = copter.ad._marker_distance_to_center.x;
	float dist_y = copter.ad._marker_distance_to_center.y;
	uint8_t _enable_pitch = 1;
	uint8_t _enable_roll = 1;
	if (dist_y <= 50.0 && dist_y >= -50.0)
	{
		_enable_pitch = 0;
	}
	if (dist_x <= 50.0 && dist_x >= -50.0)
	{
		_enable_roll = 0;
	}
	if(_enable_pitch == 0 && _enable_roll == 0)
	{
		attitude_control->input_euler_angle_roll_pitch_yaw(0.0, 0.0, 0.0, true);
		pos_control->update_z_controller();
		std::cout << "GOT IT!!!" << std::endl;
		_on_top_of_marker = true;
		return;
	}
	// float roll_in;
	// float pitch_in;

	// x - roll, y - pitch, z - throttle

	// float target_position_x = _x_real;
	// float target_position_y = _y_real;

	// std::cout << target_position_x << std::endl;
	// std::cout << target_position_y << std::endl;

	float roll_in = _enable_roll * dist_x * (-0.1); // in centidegree, roll + : fly right
	// float roll_in = 500.0;
	float pitch_in = _enable_pitch * dist_y * (-0.1); // in centidegree, pitch + : fly reverse
	// float pitch_in = 0.0;

	std::cout << roll_in << std::endl;
	std::cout << pitch_in << std::endl;

	attitude_control->input_euler_angle_roll_pitch_yaw(roll_in, pitch_in, 0.0, true);
	pos_control->update_z_controller();
}