#pragma once

#include <GCS_MAVLink/GCS.h>

#include <string>
#include <vector>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/features2d/features2d.hpp>

#include <gazebo/gazebo_config.h>
#include <gazebo/gazebo_client.hh>
#include <gazebo/msgs/msgs.hh>
#include <gazebo/transport/transport.hh>

class CameraIntegration
{
private:
	static CameraIntegration *_singleton;

public:
	// Source OpenCV
	cv::VideoCapture *_cap;
	cv::Mat *_image;

	// Source image from Gazebo topics
	gazebo::transport::NodePtr _node;
	gazebo::transport::SubscriberPtr _sub;

	// Static Result Image
	static cv::Mat _RESULT_IMAGE;
	static bool _RESULT_IMAGE_SET;
	static uint8_t _RESULT_IMAGE_LOCK;

	// Published Image
	cv::Mat _post_image;
	bool _post_image_set;

	// Camera Info
	double _height;
	double _width;
	uint8_t _distortion_model;
	cv::Mat _distortion_coefficient;
	cv::Matx33d _intrinsic_matrix;

	CameraIntegration()
	{

		_RESULT_IMAGE_SET = false;

		gazebo::client::setup();
		_node = gazebo::transport::NodePtr(new gazebo::transport::Node());

		_node->Init();
		_sub = _node->Subscribe("/gazebo/default/fake_streaming", CameraIntegration::GET_IMAGE);

		_singleton = this;
		_post_image_set = false;

		_RESULT_IMAGE_LOCK = 0;
	}

	static CameraIntegration *get_singleton()
	{
		return _singleton;
	}

	void init(uint32_t index);
	void update();

	static void GET_IMAGE(ConstImagePtr &msg)
	{
		if (_RESULT_IMAGE_LOCK == 2)
		{
			return;
		}

		_RESULT_IMAGE_LOCK = 1;

		int width;
		int height;
		char *data;

		width = (int) msg->width();
		height = (int) msg->height();
		data = new char[msg->data().length() + 1];

		memcpy(data, msg->data().c_str(), msg->data().length());
		cv::Mat image(height, width, CV_8UC3, data);

		CameraIntegration::_RESULT_IMAGE = image.clone();

		delete data;

		_RESULT_IMAGE_LOCK = 0;
		_RESULT_IMAGE_SET = true;
	}

	void setCameraInfo(double height, double width, double k1, double k2, double p1, double p2, double f, double cx, double cy);
};