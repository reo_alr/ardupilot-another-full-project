#include "NEW_Camera.h"
#include <string>

#define IMAGE_TOPIC "/gazebo/default/fake_streaming"

void CameraIntegration::setCameraInfo(double height, double width, double k1, double k2, double p1, double p2, double f, double cx, double cy)
{
	_height = height;
	_width = width;

	_intrinsic_matrix = cv::Matx<double, 3, 3>(f, 0, cx, 0, f, cy, 0, 0, 1.0);

	// double arr[4] = { k1, k2, p1, p2 };
	_distortion_coefficient = (cv::Mat_<double>(1, 4) << k1, k2, p1, p2);
}

void CameraIntegration::update()
{
	// Mat frame;
	// _cap >> frame;
	// _frame = &frame;
	// gcs().send_text(MAV_SEVERITY_INFO, "Check Image");
	if (! _RESULT_IMAGE_SET)
	{
		return;
	}
	// gcs().send_text(MAV_SEVERITY_INFO, "Image get");
	if (_RESULT_IMAGE_LOCK == 1)
	{
		return;
	}
	_RESULT_IMAGE_LOCK = 2;

	_post_image = _RESULT_IMAGE.clone();
	_post_image_set = true;
	
	_RESULT_IMAGE_LOCK = 0;
}

void CameraIntegration::init(uint32_t index)
{
	return;
}

CameraIntegration *CameraIntegration::_singleton;
cv::Mat CameraIntegration::_RESULT_IMAGE;
uint8_t CameraIntegration::_RESULT_IMAGE_LOCK;
bool CameraIntegration::_RESULT_IMAGE_SET;