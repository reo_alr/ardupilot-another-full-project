#pragma once

#include <math.h>

#include <GCS_MAVLink/GCS.h>

#include <opencv2/opencv.hpp>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/aruco.hpp>

#include "NEW_Camera/NEW_Camera.h"

class ArucoDetector
{
private:
	static ArucoDetector *_singleton;

public:
	CameraIntegration *_cam;
	// Vector3f _detected_pose_from_marker;
	float _marker_size; // TEST: 0.1778

	// Output
	uint32_t _detected_marker_id;
	bool _marker_found;

	// Aruco parameters
	cv::Ptr<cv::aruco::DetectorParameters> _detector_params;
	cv::Ptr<cv::aruco::Dictionary> _dictionary;

	// Target position and orientation
	bool _target_set;
	cv::Vec3d _target_position;
	cv::Vec3d _target_orientation;
	cv::Point2f _marker_distance_to_center;

	ArucoDetector()
	{
		_singleton = this;
		_cam = CameraIntegration::get_singleton();
		// _marker_size = marker_size;

		_dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_ARUCO_ORIGINAL); // TODO: get a better dictionary
		_detector_params = cv::aruco::DetectorParameters::create();
		// _detector_params->cornerRefinementMethod = cv::aruco::CORNER_REFINE_SUBPIX;
		_detector_params->adaptiveThreshWinSizeMin = 5;
		_detector_params->adaptiveThreshWinSizeMax = 21;

		// std::cout << "hol up" << _detector_params->adaptiveThreshWinSizeMin <<std::endl;

		_marker_found = false;

		_target_set = false;
	}

	static ArucoDetector *get_singleton()
	{
		return _singleton;
	}

	void update();
	void init(float marker_size);

	static void getEulerAngles(cv::Vec3d &rotCamerMatrix, cv::Vec3d &eulerAngles)
	{
		cv::Mat rotCamerMatrix1;
		cv::Rodrigues(rotCamerMatrix,rotCamerMatrix1);
		cv::Mat cameraMatrix,rotMatrix,transVect,rotMatrixX,rotMatrixY,rotMatrixZ;
		double* _r = rotCamerMatrix1.ptr<double>();
		double projMatrix[12] = {_r[0],_r[1],_r[2],0, _r[3],_r[4],_r[5],0, _r[6],_r[7],_r[8],0};
		cv::decomposeProjectionMatrix(cv::Mat(3,4,CV_64FC1,projMatrix), cameraMatrix, rotMatrix, transVect, rotMatrixX, rotMatrixY, rotMatrixZ, eulerAngles);
	}

	static cv::Point2f markerCenter(std::vector<cv::Point2f> corners)
	{
		// aruco marker corners: upper left, upper right, lower right, lower left
		cv::Point2f corner_1 = corners[0];
		cv::Point2f corner_3 = corners[2];
		float center_1_x = corner_1.x + ((corner_1.x - corner_3.x) / 2.0);
		float center_1_y = corner_1.y + ((corner_1.y - corner_3.y) / 2.0);

		cv::Point2f corner_2 = corners[1];
		cv::Point2f corner_4 = corners[3];
		float center_2_x = corner_2.x + ((corner_2.x - corner_4.x) / 2.0);
		float center_2_y = corner_2.y + ((corner_2.y - corner_4.y) / 2.0);

		// combine
		float approx_center_x = (center_1_x + center_2_x) / 2.0;
		float approx_center_y = (center_1_y + center_2_y) / 2.0;

		return cv::Point2f(approx_center_x, approx_center_y);
	}

	static cv::Point2f markerDistanceFromCenter(std::vector<cv::Point2f> corners, uint32_t height, uint32_t width)
	{
		cv::Point2f marker_center = ArucoDetector::markerCenter(corners);
		float center_x = width / 2;
		float center_y = height / 2;
		return cv::Point2f(center_x - marker_center.x, center_y - marker_center.y);
	}
};